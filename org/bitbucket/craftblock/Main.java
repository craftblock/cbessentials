package org.bitbucket.craftblock;

import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	public final Logger logger = Logger.getLogger("Minecraft");
	
	@Override
	public void onDisable() {
		logger.info("CBEssentials is now disabled.");
	}
	
	@Override
	public void onEnable() {
		logger.info("CBEssentials is now enabled.");
	}

}